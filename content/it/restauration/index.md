---
title: "Restauro"
slug: "restauro"
description: "Pasti e bevande"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 2
---

<!--
Dalle 11.00 Ginette vi servirà nel nostro grande spazio RISTORANTE

- Pissaladière

- Ravioli di borragine

- Piatto _Transhumance_

- Ratatouille

- Pan bagnat

- Panino, salsicce, merguez

![Ginette, responsabile del catering](/images/ginette.png)

-->