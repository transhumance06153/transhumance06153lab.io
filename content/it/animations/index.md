---
title: "Animazioni"
slug: "animazioni"
description: "Animazioni i lotteria"
featured_image: "/images/bandeau-accueil.jpg"
tags: ["Animazione", "Lottera"]
menu:
  main:
    weight: 4
---


## Animazioni

<!--

* Guida delle oche con Emmanuel DURST

* Area giochi per bambini con Denis AVON

* Concorso di disegno per bambini di 6/12 anni con l'associazione "BULLE D'ARTS".

* Pittura del viso per bambini, 1 gettone

* Preservazione degli spazi naturali con il Parco Nazionale di MERCANTOUR

* Cani da protezione con l'associazione "ON VOUS DIT PATOU"

* Gestione dei pascoli di montagna con il CERPAM

* Storia e necessità della transumanza con Eric GILLI, Édition de l'AMONT

* Come fare il compost in casa con l'Associazione COMPOST'N-CO

* Scambio sullo sviluppo sostenibile con il "Collectif transition en VÉSUBIE - VALDEBLORE".

* Racconto di storie con Jacques DROUIN e Christian LORENZETTI.

* Grande TOMBOLA con gli espositori del mercato contadino - ore 15.00

* Giro in carrozza a cavallo 10€ con La FERME DU MERCANTOUR

* Cavalcata su pony con il Centre Equestre du MERCANTOUR
-->
![Un border collie che raduna le pecore](/images/border-collie-working.jpg)

Appuntamento a Valdeblore il sabato 29 giugno