---
title: "Chi siamo ?"
slug: acerda
description: "Una squadra di appassionati"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 3
---
![Foto di una squadra di appassionati](/images/equipe_valdepom.jpg)

La transumanza in Valdeblore è nata dall'idea di un membro del trio fondatore di Valdepom.

Torniamo indietro nel tempo.

Nel 2018, i membri della sezione Colibris del defunto Pierre Rahbi hanno notato una grande quantità di mele che sono state perse in autunno. Dopo un lungo processo, i Colibrì hanno ottenuto un finanziamento dal Parco del Mercantour.

Un frantoio, una pressa e uno sterilizzatore sono stati quindi dati ai tre volontari di Valdepom per realizzare il progetto di pressatura delle mele.
Yves David, Arthur Sorridente e Yves Feraud mobiliteranno molti proprietari per produrre più di 1000 litri di succo di mela nel 2020.

Da allora, Valdepom ha acquistato le proprie attrezzature, una trentina di volontari ci accompagnano nelle nostre operazioni e l'associazione ha più di 100 membri.
Essendo la storia bella, il pastore della squadra proporrà di riunire tutti gli animali presenti sul nostro comune di montagna.

Nasce la transumanza in Valdeblore.

Per l'edizione 2024, il comitato organizzatore è il seguente

_Presidente_ : Yves Feraud

_Tesoriere_ : Christine Feraud, _Vice Tesoriere_ : Yves David

_Segretario_ : Yves Feraud, _Segretario assistente_: Annie Belloc

_Responsabile tecnico_ : Bertrand Belloc

_Responsabile del catering_ : Ginette Comte

Il Presidente e la sua squadra vi ringraziano per la vostra partecipazione e vi augurano una buona giornata a Valdeblore

Grazie anche a tutti i volontari, partner ed espositori che contribuiscono al successo di questo festival della transumanza.

![Foto di volontari 2022](/images/benevoles2022.jpeg)
