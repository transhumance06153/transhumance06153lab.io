---
title: Avvisi legali
slug: avvisi-legali
featured_image:
omit_header_text: true
description:
type: page
---
# Informazioni editoriali

## Editore

VALDEPOM'

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

SIRENA 890 330 574

Associazione dichiarata con il numero W062016706 nell'elenco nazionale delle associazioni

## Direttore della pubblicazione

Yves David, presidente dell'associazione

# Protezione e trattamento dei dati personali

## Conformità alle leggi vigenti

Il sito transhumance-en-valdeblore.fr rispetta la privacy dell'utente Internet e si attiene rigorosamente alle leggi in vigore sulla protezione della privacy e delle libertà individuali. Nessuna informazione personale viene raccolta a vostra insaputa. Nessuna informazione personale viene trasmessa a terzi. Le e-mail, gli indirizzi elettronici o altre informazioni personali ricevute da questo sito non vengono utilizzate e sono conservate solo per il tempo necessario al loro trattamento.

## Diritti degli utenti di Internet: diritto di accesso e rettifica

L'associazione si impegna a garantire che la raccolta e il trattamento dei vostri dati dal sito sia conforme al regolamento generale sulla protezione dei dati (RGPD) e alla legge sulla protezione dei dati.

Per qualsiasi informazione o per esercitare i suoi diritti relativi al trattamento dei dati personali, può contattare il suo responsabile della protezione dei dati (DPO) via e-mail all'indirizzo rgpd@transhumance-en-valdeblore.fr o per posta al seguente indirizzo

VALDEPOM'

All'attenzione del responsabile della protezione dei dati (DPO)

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

La politica del sito transhumance-en-valdeblore.fr è conforme alla legge n°2004-575 del 21 giugno 2004 per la fiducia nell'economia digitale.

## Biscotti

Quando visitate il nostro sito web transhumance-en-valdeblore.fr, dei cookies sono collocati sul vostro computer, sul vostro cellulare o sul vostro tablet.

Gli unici cookie utilizzati dal sito sono quelli destinati alla misurazione del pubblico e non raccolgono dati personali. Gli strumenti di misurazione del pubblico sono impiegati per ottenere informazioni sulle abitudini di navigazione dei visitatori. In particolare, permettono di capire come gli utenti arrivano su un sito e di ricostruire il loro percorso.

I dati generati dai cookie sono trasmessi e conservati dai fornitori di servizi di misurazione del pubblico (Xiti). I fornitori di servizi di misurazione del pubblico possono comunicare questi dati a terzi in caso di obbligo legale o quando questi terzi elaborano questi dati per loro conto.

### Definizione di cookie

Un cookie è un file di testo depositato sul suo computer quando visita un sito o visualizza una pubblicità. Il suo scopo è quello di raccogliere informazioni relative alla sua navigazione e di inviarle servizi adatti al suo terminale (computer, cellulare o tablet). I cookie sono gestiti dal vostro browser internet.

Nella misura del possibile, ci assicuriamo che i fornitori di servizi di misurazione del pubblico rispettino rigorosamente il Data Protection Act francese del 6 gennaio 1978 e successive modifiche e si impegnano a mettere in atto misure adeguate per garantire e proteggere la riservatezza dei dati.

### Impostazione del browser internet

Può scegliere di disattivare questi cookie in qualsiasi momento. Il suo browser può anche essere impostato per notificarle i cookie che vengono inseriti nel suo computer e chiederle di accettarli o meno. Potete accettare o rifiutare i cookies caso per caso o rifiutarli sistematicamente.

Se rifiutate di accettare un cookie, non potrete consultare il sito transhumance-en-valdeblore.fr. Al fine di gestire al meglio i cookie, vi invitiamo a configurare il vostro browser in base allo scopo dei cookie.

# Informazioni sul fornitore di servizi

L'hosting è fornito dalla società [Gitlab](https://about.gitlab.com/company/).

Lo sviluppo è fornito dalla società [Azimut](https://azimut.fr) con il framework [HUGO](https://gohugo.io/) e il [tema Ananke](https://github.com/theNewDynamic/gohugo-theme-ananke).

# Copyright e diritti di riproduzione

## Diritti di riproduzione dei documenti

Tutti i contenuti del sito sono coperti da copyright. Ogni riproduzione è quindi soggetta all'accordo dell'autore ai sensi dell'articolo L.122-4 del Codice della proprietà intellettuale. L'informazione usata deve essere solo per scopi personali, associativi o professionali; qualsiasi distribuzione o uso per scopi commerciali o pubblicitari è proibito.

## Richiesta di autorizzazione a riprodurre il logo

Se, nell'ambito di qualsiasi comunicazione, avete bisogno di utilizzare il logo dell'associazione per qualsiasi media interno o esterno (opuscoli, pubblicazioni, siti web, ecc.), vi preghiamo di inviare la vostra richiesta al seguente indirizzo

VALDEPOM'

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

## Richiesta di autorizzazione a riprodurre il contenuto

Qualsiasi copia parziale o completa su carta o in forma elettronica di pagine del sito deve essere dichiarata al webmaster.

Le richieste di autorizzazione alla riproduzione dei contenuti devono essere indirizzate alla redazione del sito web dell'associazione, scrivendo a

VALDEPOM'

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

La richiesta deve specificare il contenuto in questione e la pubblicazione o il sito su cui apparirà. Una volta ottenuta questa autorizzazione, la riproduzione del contenuto deve rispettare i seguenti principi

   - distribuzione gratuita ;
   - rispetto dell'integrità dei documenti riprodotti (nessuna modifica o alterazione di nessun tipo)
   - menzione obbligatoria: "© Valdepom' - tutti i diritti riservati". Questa menzione punterà direttamente al contenuto per mezzo di un link ipertestuale.

## Crediti fotografici e illustrazioni

Tutte le fotografie del sito sono pubbliche o di proprietà dell'associazione © Valdepom'

## Creare link al sito

Il sito transhumance-en-valdeblore.fr autorizza, senza autorizzazione preliminare, la creazione di collegamenti ipertestuali verso le sue pagine, alle seguenti condizioni

   - non utilizzare la tecnica del deep link, cioè le pagine del sito transhumance-en-valdeblore.fr non devono essere incorporate nelle pagine di un altro sito, ma visibili aprendo una finestra indipendente.
   - menzionare la fonte, che punterà direttamente al contenuto in questione per mezzo di un link ipertestuale.
   - non usare il logo dell'associazione senza permesso

I siti che scelgono di collegarsi a transhumance-en-valdeblore.fr sono responsabili se danneggiano l'immagine del sito pubblico.

Questo sito è "responsive" per essere visualizzato sullo schermo di un computer, tablet o smartphone.
