---
title: "Programma"
slug: "programma"
description: "Corso del giorno"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 1
---

Valdeblore, un comune situato tra 399 e 2674 m di altitudine, ha tutti gli animali transumanti.

In questa giornata, le 350 pecore del GAEC des Éleveurs du Baous
<!--, i cavalli del Centre équestre du Mercantour, le capre di Lionel, le mucche della fattoria del Mercantour e gli asini delle ânes de blore sfileranno tra i villaggi della Bolline-->
sfileranno tra il Bois Noir e il Soum del Pra a St Dalmas de Valdeblore.


![un gregge di pecore](/images/ovale_troupeau.png)

