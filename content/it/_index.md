---
title: "Transumanza  in Valdebore 2024"
slug: "transumanza-valdebore-2024"

description: "Accompagneremo i nostri allevatore il sabato 29 giugno."
cascade:
  featured_image: '/images/bandeau-accueil.jpg'
---
<p>
<iframe src="https://www.azimut.fr/videos/2022-06-27fr3.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Servizio FR3 Côte d'Azur trasmesso il 27 giugno 2022</iframe>
</p>

### Transumanza...

Transumanza ovina dalla Provenza alle Alpi è il titolo di un opuscolo molto completo, illustrato con mappe e foto, offerto dalla Maison de la transhumance, situata a Salon de Provence:

https://www.transhumance.org/

La parola transumanza integra due informazioni, "trans" (al di là) e "humus" (il paese), perché il viaggio che designa porta al di là del territorio di origine.

Gli autori spiegano come la transumanza sia un modello di complementarità tra montagna e pianura. Vengono presentate le molteplici forme di transumanza.

Questo tipo di allevamento, regolato dai cicli naturali dell'erba e dell'animale, garantisce una produzione di alta qualità.

Pur adattandosi ai cambiamenti della società, gli allevatori sono riusciti a mantenere il carattere autentico dell'allevamento.

Una mappa mostra i luoghi di villeggiatura dalle pianure ai massicci del Mercantour e del Monte Bianco.

![Mappa della transumanza delle pecore](/images/carte-transumanze-ovine.png)

Viene presentata la professione del pastore, con il suo specifico know-how.
Dalle pianure costiere alle montagne alpine, i pastori transumanti utilizzano pratiche ecologiche.

Viene anche affrontato il tema dell'allevamento pastorale minacciato dai lupi.
Infine, è in fase di realizzazione un percorso agrituristico transfrontaliero: "La Routo. Sulle orme della transumanza"

https://larouto.eu/

Scarica la brochure:

https://www.transhumance.org/documentation-et-telechargement/


Informazioni distribuite per gentile concessione della Maison de la Transhumance.


![Logo CERPAM](/logos/logo_cerpam64.png)
Uno studio simile è proposto dal CERPAM, Centre d'Etudes et de Réalisations Pastorales Alpes-Méditerranée: "une transhumance, des transhumances":

https://cerpam.com/les-transhumances/


Anne-Marie Destefanis
