---
title: "Partners"
slug: partners
description: "I nostri partner e sostenitori"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 5
---

# Transumanza in Valdeblore

## 29 giugno 2024

Grazie per la vostra partecipazione, il vostro aiuto e il vostro sostegno.

### Allevatori  :

- [Le GAEC des éleveurs des Baous](https://www.facebook.com/gaec.eleveursdesbaous)
<!--
- [Les fromages de Lionel](https://hoodspot.fr/eleveur-d-ovins-et-de-caprins/les-fromages-de-lionel-83021116500014/)
-->

### Inserzionisti :

- [Le Trait d’union](https://traitdunion06.wordpress.com/)
- [Le Portail Vésubien](http://www.vesubian.com/index.html)
- [Nice-Matin - D. GASTALDI](https://www.nicematin.com/commune/valdeblore)
- [OTM la Colmiane Valdeblore](https://www.colmiane.com/)
- [France Bleu Azur](https://www.francebleu.fr/provence-alpes-cote-dazur/alpes-maritimes-06/valdeblore-06153)
- [Radio Emotion](https://radioemotion.fr/)

### Bancas

- [Crédit  Agricole Provence - Côte d'Azur](https://www.credit-agricole.fr/ca-pca)

### Autorità locali :

- [Mairie de Valdeblore](https://ville-valdeblore.fr/)
- [Mairie de Rimplas](https://www.ville-rimplas.fr/)
- [Métropole Nice-Côte d’Azur](https://www.nicecotedazur.org/contenu/les-communes/valdeblore/43)
- [Conseil départemental des Alpes-Maritimes](https://www.departement06.fr/patrimoine-par-commune/valdeblore-2338.html)
- [Région PACA](https://www.maregionsud.fr/)

Tutti i nostri espositori

Tutti i nostri visitatori

E tutti i dinamici volontari della Transhumance en Valdeblore.