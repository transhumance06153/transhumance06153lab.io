---
title: "Mercato della transumanza"
slug: "mercato-transumanza"
description: "Gli espositori"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 2
---

# Mercato della transumanza 2024

<!--
## FORMAGGI

Andrea Colombero (_Vacherie du Collet_), formaggio di latte vaccino

Lionel Rezio (_Les fromages de Lionel_), formaggio di capra

Valentin Sic (_Les fromages de la Dorgane_), formaggio di pecora

Claire Trastour (_GAEC Éleveurs des baous_), formaggio - salumeria


## STORIE

Jacques Drouin, narratore e scrittore

Christian Lorenzetti, narratore


## SCRITTORI

Corinne Paolini (_Carnet d'inspiration du Mercantour_), scrittrice

Malou Ravella, albi per bambini, scrittore


## CIBO

Carole Arogou (_À Croquer_), pasticcera

Élise Augier (_Domaine Augier_), vini

Nathalie Bagnus (_La petite ferme_), prodotti di fattoria

Patricia e Patrice Bounichou, Johann Roth (_Ô délices de la Gi_), torte dolci e salate

Fred Bruno (Val d'abeille), miele

Cathy Carizzoni (Les jardins du blé vert), ortaggi e verdure

Mélanie Cassard (_La cabane à safran_), verdure, uova, zafferano

Guilhem Codera (_Terroir 06_), olive e agrumi

Sarah de Carqueray (_Sarah et Nicolas_), marmellata e gelato

Sandrine Crisci (_Confiserie Crisci_), torrone e pralina

Thomas Dubreuck (_La cerf-voise_), birra

Élise Durand (_Des abeilles et des fruits_), frutti rossi e miele

Marion Favaro (_L'oustal de Marion_), marmellata e miele

Jean-Pierre Fenocchio (_Côteaux Saint-Paulois_), marmellata e succo di mela

Jéremy Laurenti (Chez Marco), salumeria

Sylvain Marie (La Ruche de Valdeblore), miele

Alain Milano (_Sobal-J_), aperitivo/bagnet

Fabienne Ramin (Le torte di Fabienne), torte

Philippe Raymondo, marmellata, uova

Geneviève Richard (_Via florae_), piante medicinali

Amélie Rigot (_Les utopistes_), verdure

Dominique Foucard (_La Ferme du Mirail_), verdure

Céline Rosenweig (_Produits locaux de la Tinée_), zafferano

Romane Sattamino (_Les ailes rouges_), miele allo zafferano

Céline Tamain (_L'univers d'Artemis_), piante medicinali

Véronique Thirion (_Aux escargots gourmands_), lumache

Mary Voarino (_Naturo'life_), tisane del Mercantour

Charlène Davy (_Les herbes folles_), marmellate e zuppe

Alice Lemut (_Un brin de Provence_), marmellate

Paul & Stark (_Chocolaterie du Pays Nicois_), cioccolato

Yves Scarsini (_L'olivette_), olive e prodotti a base di olive


## DECORAZIONE - SALUTE

Stéphane Aimé (_La galerie de topa_), bastoncini scolpiti

Michelle Arsan (_Les détours du bois_), decorazione e giocattoli in legno

Gregory Biagioli (_Paracorde_), prodotti in corda

Virginie Barbera (_Vithal'créa & mimosas_), decorazione

Christine Bertho (_Crystal création_), bigiotteria

Chantal Blais, pittura su legno

Amandine Digel (_Les ânes de Lara_), sapone

Véronique Dutay (_Au gré des bois_), oggetti/pittura su legno

Emanuel Gomes (_Cocolive dal Brasile_), legno di ulivo

Patricia Hulin (_Atelier Abella_), lampade e oggetti decorativi

Nadine Jeannot, dipinti di animali

Caroline Lelo, natura sotto vetro, terrario

Nicolas Merlin (_Savonnerie de la Tinée_), sapone

Lila Ohlalalila, ceramica e artigianato

Jacqueline Pastori, decorazione d'interni, fasce per capelli, scrunchies

Aline Sigust, oggetti decorativi

Jean Pierre Vautherot, intreccio di cesti

Tanya Zollino (_Arts & Stones_), gioielli

Guillaume (_Le lycée de la Montagne_), lavorazione del legno

Christine Begole (_Le cadeau du cerf_), oggetti decorativi


## LANA

Patricia Chalot (_Na Va Ma_), infeltrimento

Nathalie Gastaldi (_Nata'laine_), lana e matasse

Mireille Marin (_Mimidou_), animali all'uncinetto

Lydia Navez (_Les dentellières du Mercantour_), pizzi e merletti

## ORTAGGI

Claude Antoniazzi (_Pépiniére de la Madone_), vivaista

## VIVERE IN MODO SOSTENIBILE

Cathy & Sandra (_Friperie des Vallées_), abiti di seconda mano

Lisa, Eric, Pierre-Antoine (_Collectif transition en Vésubie - Valdeblore_), transizione sostenibile nelle nostre valli

## TEMPO LIBERO

Bernard Baldassare (Les chemins d'azur), accompagnatori di montagna

## EVENTI - ISTITUZIONI

Denis Avon (_La ludothèque_), giochi per bambini

Martine Donato (_Les bulles d'arts_), disegno e pittura del viso

Emmanuel Durst, guida delle oche

Virginie Favier (_Compost'n Co_), compostaggio

Marie Gonthier (CERPAM), gestione della montagna

Valentine Guerin (_On vous dit Patou_), informazioni sui patoi

Romain Lacoste (Parco nazionale del Mercantour), ambiente

Jessica, (_Jeunes Agriculteurs d'Alpes Maritimes_), associazione professionale degli agricoltori
-->

<!---
<iframe src="https://www.azimut.fr/videos/video.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Presentazione dei produttori del mercato della transumanza 2022 1/2</iframe>.

<iframe src="https://www.azimut.fr/videos/video2.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Presentazione dei produttori del Mercato della Transumanza 2022 2/2</iframe>
</p>
--->


![un produttore di formaggio](/images/fromager.png)