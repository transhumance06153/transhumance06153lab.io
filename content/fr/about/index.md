---
title: "Qui sommes-nous ?"
slug: a-propos
description: "Une équipe de passionnés"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 6
---
![Photo d'une équipe de passionnés](/images/equipe_valdepom.jpg)

La Transhumance en Valdeblore est née de l’idée d’un membre du trio fondateur  de Valdepom.

Remontons le temps.

En 2018, les membres de la section Colibris du regretté Pierre Rahbi constataient  une grande quantité de pommes qui se perdaient  l’automne venu. Après de longues démarches les Colibris obtenaient un financement du Parc du Mercantour.

Broyeur, pressoir et stérilisateur étaient donc remis aux trois bénévoles de Valdepom  pour mener à bien le projet de presser les pommes.
Yves David, Arthur Sorridente et Yves Feraud mobiliseront de nombreux propriétaires afin de réaliser plus de 1000 litres de jus de pomme en 2020.

Depuis Valdepom a acheté son propre matériel, une trentaine de bénévoles nous accompagne dans nos opérations et l’association compte plus de 100 membres.
L’histoire étant belle, le berger de l’équipe proposera de réunir tous les animaux présents sur notre commune montagnarde.

La transhumance en Valdeblore est née.

Pour l’édition 2024, le bureau organisateur est ainsi constitué :

_Président_ : Yves Féraud

_Trésorière_ : Christine Feraud, _Trésorier Adjoint_ : Yves David

_Secrétaire_ : Yves Feraud, _Secrétaire Adjointe_ : Annie Belloc

_Responsable Technique_ : Bertrand Belloc

_Responsable Restauration_ : Chistine Feraud

Le Président et son équipe vous remercient de votre participation et vous souhaite une belle journée en Valdeblore.

Remerciements également à tous les bénévoles, les partenaires ainsi que nos exposants qui contribuent à la réussite de cette fête de la transhumance.

![Photo des bénévoles 2022](/images/benevoles2022.jpeg)