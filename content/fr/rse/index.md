---
title: "RSE"
slug: "rse"
description: "Responsabilité Sociétale de l'Évènement"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 3
---

La responsabilité sociétale des entreprises (RSE) se définit comme la contribution volontaire des entreprises aux enjeux du développement durable, aussi bien dans leurs activités que dans leurs interactions avec leurs parties prenantes.

Elle concerne trois domaines :  environnemental, social et sociétal.

Nous l'avons reprise à notre compte pour l'appliquer à la fête de la transhumance.

C'est dans cet esprit que nous faisons évoluer notre événement pour :

- réduire nos déchets
- favoriser la participation de tous
- encourager et mettre en avant, au même titre que nos éleveurs, les organismes ou projets agissant pour un mode de vie et/ou un développement plus durable

Afin de réduire drastiquement nos déchets, nous supprimons les canettes de
 notre offre et vous proposerons uniquement des boissons servies _au verre_. Pour les mêmes raisons, nous vous servirons repas et boissons
dans de la vaisselle, certes en matériau polymère, *mais réutilisable et qui 
sera donc consignée*. Cette consigne vous sera rendue à la restitution des 
assiettes, couverts et autres gobelets. 

C'est aussi pourquoi il n'y aura pas de nappes en papier sur les tables où vous pourrez vous restaurer.

Pour des raisons pratiques, nous utiliserons encore des gobelets en carton pour le service du café.

Nous vous proposerons également un sac en tissu pour transporter votre vaisselle consignée et vos achats à 
nos producteurs.

Par ailleurs, nous souhaitons éviter au maximum vos temps d'attente. A ce
 titre, nous avons mis en place un système de paiement par jetons qui 
 simplifiera les transactions. Vous serez donc invités à acheter des 
jetons à votre arrivée au Soun del Pra. Vous pourrez régler ces jetons avec 
des espèces ou avec votre CB (*pas de montant minimal*).

Ces jetons vous permettront de payer vos consommations sur notre espace 
Restauration (repas /boissons). En revanche, vous ne pourrez pas les utiliser 
pour régler vos achats à nos producteurs.

Lorsque vous quitterez la fête, vous pourrez vous faire rembourser les jetons inutilisés.