---
title: "Transhumance en Valdeblore 2024"
slug: "transhumance-en-valdeblore-2024"
description: "Retrouvez le troupeau des Éleveurs des Baous le samedi 29 juin."
cascade:
  featured_image: '/images/bandeau-accueil-2024.jpg'
---
<!--
<p>
<iframe src="https://photos.transhumance-en-valdeblore.fr/videos/2022-06-27fr3.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Reportage FR3 Cote d'Azur diffusé le 27 juin 2022</iframe>
</p>
-->

<iframe src="https://photos.transhumance-en-valdeblore.fr/index.html" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true">Retrouvez les photos de notre reporter ici</iframe>

### Transhumance...

Les transhumances ovines de la Provence aux Alpes, c’est le titre d’une brochure très complète, illustrée de cartes et photos que propose la Maison de la transhumance, située à Salon de Provence :

https://www.transhumance.org/

Le mot transhumance intègre deux informations, "trans" (au-delà) et "humus" (le pays), car le voyage qu’il désigne conduit au-delà du territoire d’origine.

Les auteurs nous expliquent en quoi la transhumance est un modèle de complémentarité entre la montagne et la plaine. Les multiples formes de transhumances sont présentées.

Ce type d’élevage rythmé par les cycles naturels de l’herbe et de l’animal garantit des productions de grande qualité.

Tout en s’adaptant aux évolutions de la société, les éleveurs ont su garder le caractère authentique de l’élevage.

Une carte montre les lieux d’estivage depuis les plaines jusqu’aux massifs du Mercantour et du Mont Blanc.

![Carte des transhumances ovines](/images/carte-transhumances-ovines.png)

Le métier de berger, au savoir-faire spécifique est présenté.
Des plaines du littoral aux montagnes alpines, les bergers transhumants mettent en œuvre des pratiques respectueuses de l’environnement.

La question de l’élevage pastoral menacé face aux loups est aussi abordée.
Enfin, un itinéraire agritouristique transfrontalier est en cours de mise en place : "La Routo. Sur les pas de la transhumance"

https://larouto.eu/

Télécharger la brochure :

https://www.transhumance.org/documentation-et-telechargement/


Informations diffusées avec l’aimable autorisation de la Maison de la Transhumance.


![logo du CERPAM](/logos/logo_cerpam64.png)
Une étude comparable est proposée par le CERPAM, Centre d'Etudes et de Réalisations Pastorales Alpes-Méditerranée : "une transhumance, des transhumances" :

https://cerpam.com/les-transhumances/


Anne-Marie Destefanis
