---
title: "Restauration"
slug: "restauration"
description: "Repas et boissons"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 3
---

<!--
Dès 11h Ginette vous propose sur notre grand espace RESTAURATION

- Pissaladière

- Raviolis à la bourrache

- Assiette _Transhumance_

- Ratatouille

- Pan bagnat

- Sandwich, saucisses, merguez

![Ginette, responsable de la restauration](/images/ginette.png)
-->