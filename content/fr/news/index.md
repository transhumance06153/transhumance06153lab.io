---
title: "Actualités"
sluc: "actualites"
description: "Dernières nouvelles"
featured_image: "/images/bandeau-accueil.jpg"
date: 2017-03-02T12:00:00-05:00
menu:
  main:
    weight: 5
---
<!---
<p>
</p>
--->
<p>
<iframe src="https://www.azimut.fr/videos/video.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Présentation des producteurs du marché de la transhumance 1/2</iframe>

<iframe src="https://www.azimut.fr/videos/video2.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Présentation des producteurs du marché de la transhumance 2/2</iframe>
</p>

[Reportage d'Antoinette. Merci !!!](https://antoinetteetcie.wixsite.com/antoinette/post/la-f%C3%AAte-de-la-transhumance-valdeblore-la-colmiane-26-juin-2022)


### Les traces du pastoralisme dans le langage des noms de lieux

Depuis l’âge primaire, le pastoralisme a laissé des traces dans le massif du Mercantour. Nous les retrouvons sous plusieurs formes retraçant le passé ou représentant l’actuel.  

Ces noms de lieux peuvent évoquer de façons différentes ces traces qui font l’environnement paysagé d’aujourd’hui. 

Le GR5&trade; principal chemin de **G**rande **R**andonnée traversant le massif du Mercantour, de Menton au lac Léman, "grouille" de traces de la présence d’une vie pastorale. 

En suivant un bout de sentier en partant de Saint Etienne de Tinée sur le GR5&trade; en direction de Saint Dalmas le **Selvage** (de *Silvéa* forêt) nous grimpons en direction du col d’**Anelle** (*agneau*), nous retrouvons ce nom de lieu sous d’autres formes : Agnel, anel, anéou, nom du jas ou de quartier donné par extension au sommet qui domine le bercail ou la vacherie. 

De magnifiques **alpages** s’étendent à l’horizon, bloqués par des Mélezins (forêt de mélèzes) : 

` `(**Alp, aup, alpe, arp :** *Alpage de haute montagne herbeuse, propre à la pâture, parfois pâturage avec grange (quartier).* 

Ex :   Pas de l’arpette : *passage dans le petit alpage* (Gordolasque)  

`   `L’Eissals : *abords de l’alpe* (Roubion, Tinée) se dit Eissalps dans l’Ubaye 

`   `L’alpé : *pâturage et grange* (col de la Colombière entre St Dalmas de Selvage et Bousieyas en Haute Tinée etc…), 

Il n’est pas rare en parcourant ces alpages d’apercevoir des tas de pierres souvent étendus comme d’anciens murs écroulés, en plein milieu de l’alpe. 

Il faut comprendre que la montagne n’offre que peu de terrain en lieux de pâtures. Chaque parcelle était utilisée à son maximum et pour cela, au fil des générations, les ‘’ pâtres’’ avaient l’habitude lorsqu’ils voyaient une pierre au milieu de l’herbe, de la ramasser et de la jeter sur le tas qui était à leur proximité. Ceci pour laisser la place à la végétation de pousser. 

Dans  certaines  régions,  une  légende  raconte  que  les  bergers  croyaient  que  les  pierres  se déplaçaient  la  nuit  car  dans  la  journée  n’étant  pas  sur  place,  ils  n’avaient  jamais  assisté  au phénomène. 

L’explication est simple : La pierre retirée du sol, laisse un coin de terre sans herbe. Une forte pluie lors d’un orage, la terre est lavée, une nouvelle pierre apparaît. 

Tous ces lieux d’alpages avec ces tas de pierres disposés à l’avenant du geste du berger laissent des noms tels que : 

**Clap**, **aclap** dérivé *clapier*, champ couvert de cailloux, pente d’éboulis rocheux* (le mont **clapier** en haute Gordolasque) 

Le quartier des **Claps** sur le plateau de Caussols au nord de Grasse. 

Lacs de **clapereit,** vallon de Salèse 

Quartier de **clapieras** au-dessus de Lantosque etc… 

Beaucoup de pierres étaient utilisées aussi pour la construction des reposoirs des troupeaux. En grande  partie  écroulés,  ces  reposoirs  se  reconnaissent  par  rapport  aux  **clapas**  à  leurs emplacements situés sur les abords des prés ou à proximité de l’habitat des bergers ou à leurs formes soit rondes, carrées ou rectangulaires. 

Nombreux sont les lieux de reposoirs des troupeaux, enclos permanents en pierres, parcs mobiles en bois. 

**Jas, jasse, gias** (du latin jacère) : *être couché* 

**Jalorgue, gialorgue** (haute Tinée) : **jas, gias** : *parquage,* l’**orgue** : *eau* 

**L’alpé** : *pâturage et grange* (col de la Colombière entre St Dalmas de 
Selvage et Bousieyas en Haute Tinée) 

**L’Estrop :** *Ce mot s’applique surtout aux troupeaux de brebis (strop de féa) ou de chèvres dispersés dans les champs de pâturage.* 

Vallon de l’Estrop : *affluent de gauche du haut Var, permettant d’accéder d’Esteng aux pâturages de Jialorgue* (montagne de l’Estrop) 

Tête de l’Estrop (dans le vallon de l’Ubaye) 

En **Ubac** (**versant exposé au nord**, l’inverse est l’**Adret** versant exposé au sud) du col d’Anelle, le chemin redescend sur le torrent du vallon de Gialorgues. 

Des constructions apparaissent au bord du chemin. Jadis elles servaient à abriter les bergers pendant la période des ‘’estives ‘’c’est à dire pendant que les troupeaux de bovins et d’ovins restaient à l’alpage de la fin du printemps au premières neiges d’automne. 

Ces  habitats  d’altitude  se  trouvent  en  grand  nombre  sur  l’ensemble  du  massif  du Mercantour. 

Nous retrouvons beaucoup de noms de lieux précisant ces abris : 

**Cabana, cabano, chabano** : *cabane, cahute* : *sert à désigner la cabane du berger :*  

* La cabano d’ou pastre (Tinée),  

* Bré de cabaneto (Peira cava) 

* Vallon de chabano vieio (de la vieille cabane au sud du mont Giraud, Valdeblore). 

**Cabota, chaboto**, en Tinée : *Cabane, cahute* : *petite ferme* 

`      `Les chabottes (ciabotte) quartier à l’Est de Marie, vallée de la Tinée 

**Caïola, cayola, cayolle** 

`      `Col de la Cayolle entre Esteng et Barcelonnette *: jas de pâturage supérieur avec enceinte où le troupeau passe la nuit, et l’abri de pierre du berger situé en bordure.* 

Le chemin bien tracé descend lentement et une multitude de petits sentiers viennent le rejoindre par endroit.  

Ces petits sentiers se répandent sur tout le flanc de la partie Nord du col, serrés ayant 10 à 20 centimètres d’écart. 

Ces chemins sont formés par le piétinement et le passage de milliers de petites pattes, ce sont en langage de la Tinée des (**draio**) *sentiers, voies suivies par les troupeaux.* 

D’autres formes de cette appellation **: Draille, draia.** 

Diminutif : **Drahelta, drailloun,** *sentier de moutons.* 

Dans l’ensemble du massif du Mercantour la mémoire des lieux évoque le passif de ce pastoralisme et par endroit encore maintenant, l’actif de ce qui est la présence du pastoralisme. 

La carte IGN a su garder cette mémoire. Il est très intéressant de la comprendre pour mieux lire le paysage et enrichir une balade ou une randonnée. 

(De la vallée au sommet tout à un sens **pour celui qui cherche** autre chose que l’ultime paroi à escalader ou la descente ‘’schuss’’ du dernier névé. Ce que nous voyons, ce que nous sentons, donne une signification et sa valeur véritable à notre effort physique ; le vallon, la forêt, les maigres cultures, les riches pâturages et le méchant clapier lui- même, tout parle et vit et se laisse découvrir lentement aux longues méditations alpines. 

Et les énigmes mêmes, rencontrées si nombreuses à ce jardin de mystères où chaque fleur est un mystère nouveau ouvert sur l’infini, procurent à notre esprit cette sérénité des sommets, véritable récompense de l’Alpe. 

Tout est joie ici pour celui qui ne cesse de regarder avec ‘’ces deux grands yeux de l’esprit qui voit et qui entend ‘’ *Andrieu Compan* 

*Alain PELLEGRINO, Valdeblore*

### Quelques posts sur notre page Facebook

<p>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D148167671110388%26id%3D101788185748337&show_text=true&width=500" width="500" height="781" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D147614177832404%26id%3D101788185748337&show_text=true&width=500" width="500" height="781" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D147101104550378%26id%3D101788185748337&show_text=true&width=500" width="500" height="742" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D146679144592574%26id%3D101788185748337&show_text=true&width=500" width="500" height="800" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D145884428005379%26id%3D101788185748337&show_text=true&width=500" width="500" height="665" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D144812451445910%26id%3D101788185748337&show_text=true&width=500" width="500" height="800" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D143396701587485%26id%3D101788185748337&show_text=true&width=500" width="500" height="487" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D141921988401623%26id%3D101788185748337&show_text=true&width=500" width="500" height="858" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D140993641827791%26id%3D101788185748337&show_text=true&width=500" width="500" height="800" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D138221225438366%26id%3D101788185748337&show_text=true&width=500" width="500" height="764" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D137799552147200%26id%3D101788185748337&show_text=true&width=500" width="500" height="437" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D137077152219440%26id%3D101788185748337&show_text=true&width=500" width="500" height="838" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D135413209052501%26id%3D101788185748337&show_text=true&width=500" width="500" height="800" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D134232292503926%26id%3D101788185748337&show_text=true&width=500" width="500" height="896" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D132833389310483%26id%3D101788185748337&show_text=true&width=500" width="500" height="800" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D130479089545913%26id%3D101788185748337&show_text=true&width=500" width="500" height="838" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D129886426271846%26id%3D101788185748337&show_text=true&width=500" width="500" height="572" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D129001503027005%26id%3D101788185748337&show_text=true&width=500" width="500" height="800" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D127698033157352%26id%3D101788185748337&show_text=true&width=500" width="500" height="602" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D127523419841480%26id%3D101788185748337&show_text=true&width=500" width="500" height="761" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D125955696664919%26id%3D101788185748337&show_text=true&width=500" width="500" height="781" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D123230440270778%26id%3D101788185748337&show_text=true&width=500" width="500" height="787" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D121150333812122%26id%3D101788185748337&show_text=true&width=500" width="500" height="761" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>

<p>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D116965680897254%26id%3D101788185748337&show_text=true&width=500" width="500" height="684" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
</p>
