---
title: "Partenaires"
slug: partenaires
description: "Nos partenaires et soutiens"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 5
---

# Transhumance en Valdeblore

## samedi  29juin 2024

Merci pour votre participation, votre aide et votre soutien.

### Éleveurs :

- [Le GAEC des éleveurs des Baous](https://www.facebook.com/gaec.eleveursdesbaous)
<!--
- [Les fromages de Lionel](https://hoodspot.fr/eleveur-d-ovins-et-de-caprins/les-fromages-de-lionel-83021116500014/)
-->
### Annonceurs :

- [Le Trait d’union](https://traitdunion06.wordpress.com/)
- [Le Portail Vésubien](http://www.vesubian.com/index.html)
- [Nice-Matin - D. GASTALDI](https://www.nicematin.com/commune/valdeblore)
- [OTM la Colmiane Valdeblore](https://www.colmiane.com/)
- [France Bleu Azur](https://www.francebleu.fr/provence-alpes-cote-dazur/alpes-maritimes-06/valdeblore-06153)
- [Radio Emotion](https://radioemotion.fr/)

### Banques

- [Crédit  Agricole Provence - Côte d'Azur](https://www.credit-agricole.fr/ca-pca)

### Collectivités territoriales :

- [Mairie de Valdeblore](https://ville-valdeblore.fr/)
- [Mairie de Rimplas](https://www.ville-rimplas.fr/)
- [Métropole Nice-Côte d’Azur](https://www.nicecotedazur.org/contenu/les-communes/valdeblore/43)
- [Conseil départemental des Alpes-Maritimes](https://www.departement06.fr/patrimoine-par-commune/valdeblore-2338.html)
- [Région PACA](https://www.maregionsud.fr/)

Tous nos Exposants

Tous nos Visiteurs

Et tous les dynamiques bénévoles de la Transhumance en Valdeblore.
