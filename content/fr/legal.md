---
title: Mentions légales
slug: mentions-legales
featured_image:
omit_header_text: true
description:
type: page
---
# Informations éditoriales

## Editeur

VALDEPOM'

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

SIREN 890 330 574

Association déclarée sous le numéro W062016706 au Répertoire National des Associations

## Direction de la publication

Yves David, président de l'association

# Protection et traitement de données à caractère personnel

## Respect des lois en vigueur

Le site transhumance-en-valdeblore.fr respecte la vie privée de l’internaute et se conforme strictement aux lois en vigueur sur la protection de la vie privée et des libertés individuelles. Aucune information personnelle n’est collectée à votre insu. Aucune information personnelle n’est cédée à des tiers. Les courriels, les adresses électroniques ou autres informations nominatives dont ce site est destinataire ne font l’objet d’aucune exploitation et ne sont conservés que pour la durée nécessaire à leur traitement.

## Droit des internautes : droit d’accès et de rectification

L'association s’engage à ce que la collecte et le traitement de vos données, effectués à partir du site soient conformes au règlement général sur la protection des données (RGPD) et à la loi Informatique et Libertés.

Pour toute information ou exercice de vos droits Informatique et Libertés sur les traitements de données personnelles, vous pouvez contacter son délégué à la protection des données (DPD) par courriel à rgpd@transhumance-en-valdeblore.fr ou par courrier à l’adresse suivante :

VALDEPOM'

À l’attention du délégué à la protection des données (DPD)

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

La politique du site transhumance-en-valdeblore.fr est en conformité avec la loi n°2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique.

## Cookies

Lors de la consultation de notre site transhumance-en-valdeblore.fr, des cookies sont déposés sur votre ordinateur, votre mobile ou votre tablette.

Les seuls cookies utilisés par le site sont ceux destinés à la mesure d’audience et ne collectent pas de données personnelles. Les outils de mesures d’audience sont déployés afin d’obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours.

Les données générées par les cookies sont transmises et stockées par les prestataires de mesure d’audience (Xiti). Les prestataires de mesure d’audience sont susceptibles de communiquer ces données à des tiers en cas d’obligation légale ou lorsque ces tiers traitent ces données pour leur compte.

### Définition d’un cookie

Un cookie est un fichier texte déposé sur votre ordinateur lors de la visite d’un site ou de la consultation d’une publicité. Il a pour but de collecter des informations relatives à votre navigation et de vous adresser des services adaptés à votre terminal (ordinateur, mobile ou tablette). Les cookies sont gérés par votre navigateur internet.

Nous veillons dans la mesure du possible à ce que les prestataires de mesures d’audience respectent strictement la loi informatique et libertés du 6 janvier 1978 modifiée et s’engagent à mettre en œuvre des mesures appropriées de sécurisation et de protection de la confidentialité des données.

### Paramétrer votre navigateur internet

Vous pouvez à tout moment choisir de désactiver ces cookies. Votre navigateur peut également être paramétré pour vous signaler les cookies qui sont déposés dans votre ordinateur et vous demander de les accepter ou pas. Vous pouvez accepter ou refuser les cookies au cas par cas ou bien les refuser systématiquement.

En cas de refus de depôt de cookie, vous ne pourrez pas consulter le site transhumance-en-valdeblore.fr. Afin de gérer les cookies au plus près de vos attentes nous vous invitons à paramétrer votre navigateur en tenant compte de la finalité des cookies.

# Informations Prestataires

L’hébergement est assuré par la société [Gitlab](https://about.gitlab.com/company/).

Le développement est assuré par la société [Azimut](https://azimut.fr) avec le framework [HUGO](https://gohugo.io/) et le [thème Ananke](https://github.com/theNewDynamic/gohugo-theme-ananke) .

# Droits d’auteur et droits de reproduction

## Droits de reproduction des documents

Tous les contenus présents sur le site sont couverts par le droit d’auteur. Toute reprise est dès lors conditionnée à l’accord de l’auteur en vertu de l’article L.122-4 du Code de la propriété Intellectuelle. Les informations utilisées ne doivent l’être qu’à des fins personnelles, associatives ou professionnelles ; toute diffusion ou utilisation à des fins commerciales ou publicitaires étant interdites.

## Demande d’autorisation de reproduction du logo

Si, dans le cadre d’une communication quelconque, vous avez besoin d’utiliser le logotype de l'association pour tous supports internes et externes (brochures, publications, sites, etc.), merci d’envoyer votre demande à l’adresse suivante :

VALDEPOM'

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

## Demande d’autorisation de reproduction des contenus

Toute copie partielle ou intégrale sur support papier ou sous forme électronique de pages du site doit faire l’objet d’une déclaration auprès du webmaster.

Les demandes d’autorisation de reproduction d’un contenu doivent être adressées à la rédaction du site de l'association, en écrivant à :

VALDEPOM'

555 CHEMIN DE PEYRE GROSSE

06420 VALDEBLORE

La demande devra préciser le contenu visé ainsi que la publication ou le site sur lequel ce dernier figurera. Une fois cette autorisation obtenue, la reproduction d’un contenu doit obéir aux principes suivants :

   - gratuité de la diffusion ;
   - respect de l’intégrité des documents reproduits (aucune modification, ni altération d’aucune sorte) ;
   - mention obligatoire : "© Valdepom' - tous droits réservés". Cette mention pointera grâce à un lien hypertexte directement sur le contenu.

## Crédits photos et illustrations

Toutes les photographies du site sont publiques ou propriété de l'association © Valdepom'

## Création de liens vers le site

Le site transhumance-en-valdeblore.fr autorise, sans autorisation préalable, la mise en place de liens hypertextes pointant vers ses pages, sous réserve de :

   - ne pas utiliser la technique du lien profond, c’est-à-dire que les pages du site transhumance-en-valdeblore.fr ne doivent pas être imbriquées à l’intérieur des pages d’un autre site, mais visibles par l’ouverture d’une fenêtre indépendante.
   - mentionner la source qui pointera grâce à un lien hypertexte directement sur le contenu visé.
   - ne pas utiliser le logotype de l'association sans autorisation

Les sites qui font le choix de pointer vers transhumance-en-valdeblore.fr engagent leur responsabilité dès lors qu’ils porteraient atteinte à l’image du site public.

Ce site est "responsive" pour être affiché aussi bien sur un écran d'ordinateur, tablette ou smartphone.
