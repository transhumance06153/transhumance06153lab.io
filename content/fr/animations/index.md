---
title: "Animations"
slug: "animations"
description: "Animations et tombola"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 4
---

<!--
* Conduite des oies avec Emmanuel DURST

* Espace de jeux enfants avec Denis AVON

* Concours de DESSIN 6/12 ans avec l'Association « BULLE D’ARTS »

* Maquillage pour enfant, 1 jeton

* Préservation de nos espaces naturels avec Le Parc National du MERCANTOUR

* Utilité des chiens de protection avec l'Association « ON VOUS DIT PATOU »

* Aménagement des alpages avec le CERPAM

* Histoire et nécessité de la Transhumance avec Eric GILLI, Édition de l’AMONT

* Comment faire son compost à la maison avec l'Association COMPOST’N-CO

* Échange sur le développement durable avec le « Collectif transition en VÉSUBIE – VALDEBLORE »

* Belles histoires des Conteurs avec Jacques DROUIN et Christian LORENZETTI

* Grande TOMBOLA des Exposants du marché de producteurs – 15h

* Balade en calèche 10€ avec La FERME DU MERCANTOUR

* Balade en poney avec le Centre Équestre du MERCANTOUR
-->
![un border collie rassemblant un troupeau de mouton](/images/border-collie-working.jpg)

Rendez- vous à Valdeblore le 2 juillet prochain
