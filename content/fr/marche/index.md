---
title: "Marché"
slug: marche-transhumance
description: "Les exposants"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 2
---
<p>

# Le marché de la transhumance 2024

<!--
## FROMAGES

Andrea Colombero (_Vacherie du Collet_),	fromages de vache

Lionel  Rezio	(_Les fromages de Lionel_),	fromages de chèvre

Valentin Sic (_Les fromages de la Dorgane_), fromages de brebis

Claire Trastour (_GAEC Éleveurs des baous_), fromages - charcuterie


## CONTEURS

Jacques  Drouin, conteur écrivain

Christian Lorenzetti,	conteur


## ÉCRIVAINS

Corinne Paolini	(_Carnet d'inspiration du Mercantour_),	écrivaine

Malou Ravella,	albums jeunesse,	écrivaine


## ALIMENTATION

Carole Arogou	(_À Croquer_),	pâtisserie

Élise Augier	(_Domaine Augier_), vins

Nathalie Bagnus	(_La petite ferme_),	produits de la ferme

Patricia et Patrice Bounichou, Johann Roth 	(_Ô délices de la Gi_),	gâteaux secs sucrés et salés

Fred Bruno	(_Val d'abeille_),	miel

Cathy Carizzoni	(_Les jardins du blé vert_),	légumes

Mélanie Cassard	(_La cabane à safran_),	légumes, œufs, safran

Guilhem Codera	(_Terroir 06_),	olives et agrumes

Sarah de Carqueray	(_Sarah et Nicolas_), confiture et glace

Sandrine	Crisci (_Confiserie Crisci_),	nougat, praline

Thomas Dubreuck	(_La cerf-voise_),	bière

Élise Durand 	(_Des abeilles et des fruits_),	fruits rouges et miel

Marion Favaro	(_L'oustal de Marion_), confiture et miel

Jean-Pierre Fenocchio	(_Côteaux Saint-Paulois_),	confiture, jus depomme

Jéremy Laurenti	(_Chez Marco_),	charcuterie

Sylvain Marie	(_La Ruche de Valdeblore_), miel

Alain Milano	(_Sobal-J_),	apéritif / bagnet

Fabienne Ramin	(_Les gâteaux de Fabienne_),	gâteaux

Philippe Raymondo,	confiture, œufs

Geneviève Richard	(_Via florae_),	plantes médicinales

Amélie Rigot	(_Les utopistes_),	légumes

Dominique Foucard (_La Ferme du Mirail_),	légumes

Céline Rosenweig,	(_Produits locaux de la Tinée_),	safran

Romane Sattamino (_Les ailes rouges_),	miel safran

Céline Tamain (_L'univers d'Artemis_),	plantes médicinales

Véronique Thirion (_Aux escargots gourmands_),	escargots

Mary Voarino	(_Naturo'life_),	tisane du Mercantour

Charlène	Davy (_Les herbes folles_),	confitures et soupes

Alice Lemut	(_Un brin de Provence_)	confitures

Paul & Stark	(_Chocolaterie  du Pays Nicois_),	chocolat

Yves Scarsini	(_L'olivette_),	olive et dérivés


## DÉCORATION - SANTÉ

Stéphane Aimé	(_La galerie de topa_),	bâtons sculptés

Michelle Arsan	(_Les détours du bois_),	décoration et jouets en bois

Gregory	Biagioli (_Paracorde_),	articles en corde

Virginie  Barbera	(_Vithal'créa & mimosas_),	décoration

Christine Bertho	(_Crystal création_),	bijoux fantaisies

Chantal  Blais,	peinture sur bois

Amandine Digel	(_Les ânes de Lara_),	 savon

Véronique	Dutay (_Au gré des bois_),	objet/peinture bois

Emanuel Gomes (_Cocolive du Brésil_), bois d'olivier

Patricia Hulin	(_Atelier Abella_),	lampes et objets de décoration

Nadine Jeannot,	peinture animalière

Caroline Lelo,	nature sous verre,	terrarium

Nicolas Merlin	(_Savonnerie de la Tinée_),	savon

Lila	Ohlalalila, céramique / artisanat

Jacqueline Pastori,	décoration intérieure,	bandeaux, chouchous

Aline Sigust,	objet decorations

Jean Pierre Vautherot,	vannier

Tanya Zollino (_Arts & Stones_),	bijoux

Guillaume (_Le lycée de la Montagne_),	travail du bois

Christine Begole	(_Le cadeau du cerf_),	objet de décoration


## LAINE

Patricia Chalot	(_Na Va Ma_),	feutre

Nathalie Gastaldi	(_Nata'laine_),	laine & écheveaux

Mireille Marin	(_Mimidou_),	animaux au crochet

Lydia Navez	(_Les dentellières du Mercantour_),	dentelles

## VÉGÉTAUX

Claude	Antoniazzi (_Pépiniére de la Madone_),	pépiniériste

## VIE DURABLE

Cathy & Sandra	(_Friperie des Vallées_),	vêtements de 2éme main

Lisa, Eric, Pierre-Antoine (_Collectif transition en Vésubie - Valdeblore_), transition durable dans nos vallées

## LOISIRS

Bernard Baldassare	(_Les chemins d'azur_),	Accompagnateurs En Montagne

## ANIMATIONS - INSTITUTIONS

Denis Avon	(_La ludothèque_),	jeux pour enfants

Martine Donato (_Les bulles d'arts_),	dessin et maquillage

Emmanuel Durst,	conduite d'oies

Virginie Favier 	(_Compost'n Co_),	compostage

Marie Gonthier	(_CERPAM_),	aménagement en montagne

Valentine Guerin	(_On vous dit Patou_), informations sur les patous

Romain Lacoste	(_Parc National du Mercantour_),	environnement

Jessica,	(_Jeunes Agriculteurs	d'Alpes Maritimes_), association professionnelle d'agriculteurs

-->
<!---
<iframe src="https://www.azimut.fr/videos/video.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Présentation des producteurs du marché de la transhumance 2022 1/2</iframe>

<iframe src="https://www.azimut.fr/videos/video2.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Présentation des producteurs du marché de la transhumance 2022 2/2</iframe>
</p>
--->


![Un fromager](/images/fromager.png)
