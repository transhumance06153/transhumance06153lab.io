---
title: "Programme"
slug: "programme"
description: "Déroulé de la journée"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 1
---

Valdeblore, commune située entre 399 et  2674 m d’altitude possède tous les animaux transhumants.

Ce jour-là les 350 moutons du GAEC des éleveurs du Baous
<!--, les chevaux du Centre équestre du Mercantour, les chèvres de Lionel, les vaches de la ferme du Mercantour et les Ànes de Blore défileront entre les villages de la Bolline, la Roche -->
 chemineront entre le Bois Noir et le Soun del Pra à St Dalmas.


![Un troupeau de moutons](/images/ovale_troupeau.png)