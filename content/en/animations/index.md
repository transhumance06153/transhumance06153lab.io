---
date: 2023-03-10T09:01:00-00:00
description: "Entertainment"
slug: "entertainment"
featured_image: "/images/bandeau-accueil.jpg"
tags: ["entertainment", "tombola"]
title: "Entertainment"
menu:
  main:
    weight: 4
---


## Entertainment

<!--


* Goose driving with Emmanuel DURST

* Children's play area with Denis AVON

* Drawing competition for 6/12 year olds with the "BULLE D'ARTS" association

* Face painting for children, 1 token

* Preservation of our natural spaces with the Parc National du MERCANTOUR

* Protection dogs with the association "ON VOUS DIT PATOU"

* Management of mountain pastures with CERPAM

* History and necessity of transhumance with Eric GILLI, Édition de l'AMONT

* How to make compost at home with the COMPOST'N-CO Association

* Exchange on sustainable development with the "Collectif transition en VÉSUBIE - VALDEBLORE".

* Storytelling with Jacques DROUIN and Christian LORENZETTI

* Big TOMBOLA with the farmers' market exhibitors - 3pm

* Horse-drawn carriage ride 10€ with La FERME DU MERCANTOUR

* Pony ride with the Centre Equestre du MERCANTOUR
-->

![A border collie gathering a sheep herd](/images/border-collie-working.jpg)

Rendezvous in Valdeblore on 2 July