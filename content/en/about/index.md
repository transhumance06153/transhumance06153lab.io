---
title: "About"
slug: about
description: "A team of enthusiasts"
featured_image: '/images/bandeau-accueil.jpg'
menu:
  main:
    weight: 4
---
![Photo of a team of enthusiasts](/images/equipe_valdepom.jpg)

The Transhumance in Valdeblore was born from the idea of a member of the founding trio of Valdepom.

Let's go back in time.

In 2018, the members of the Colibris section of the late Pierre Rahbi noticed a large quantity of apples that were lost in the autumn. After a long process, the Colibris obtained financing from the Mercantour Park.

A crusher, a press and a steriliser were therefore given to the three Valdepom volunteers to carry out the apple pressing project.
Yves David, Arthur Sorridente and Yves Feraud will mobilise many owners in order to produce more than 1000 litres of apple juice in 2020.

Since then, Valdepom has bought its own equipment, about thirty volunteers accompany us in our operations and the association has more than 100 members.
The story being beautiful, the shepherd of the team will propose to gather all the animals present on our mountain commune.

The transhumance in Valdeblore was born.

For the 2024 edition, the organising committee is composed as follows

_Chairman_ : Yves Feraud

_Treasurer_ : Christine Feraud, _Deputy Treasurer_ : Yves David

_Secretary_ : Jean-Marie Arsac, _Deputy Secretary_ : Annie Belloc

_Technical Manager_ : Bertand Belloc

_Catering Manager_ : Christine Feraud

The President and his team thank you for your participation and wish you a nice day in Valdeblore


![Photo of volunteers 2022](/images/benevoles2022.jpeg)