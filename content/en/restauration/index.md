---
title: "Catering"
slug: "catering"
description: "Food and drink"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 2
---

<!--
From 11am Ginette will be serving you in our large RESTAURANT area

- Pissaladière

- Ravioli with borage

- Plate _Transhumance_

- Ratatouille

- Pan bagnat

- Sandwich, sausages, merguez

[Ginette, catering manager](/images/ginette.png)
-->