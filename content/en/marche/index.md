---
title: "Transhumance market"
slug: "transhumance-market"
description: "The exhibitors"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 2
---

<p>

# Transhumance market 2024

<!--
## CHEESES

Andrea Colombero (_Vacherie du Collet_), cow's milk cheese

Lionel Rezio (_Les fromages de Lionel_), goat's cheese

Valentin Sic (_Les fromages de la Dorgane_), ewe's milk cheese

Claire Trastour (_GAEC Éleveurs des baous_), cheese - charcuterie


## STORIES

Jacques Drouin, storyteller and writer

Christian Lorenzetti, storyteller


## WRITERS

Corinne Paolini (_Carnet d'inspiration du Mercantour_), writer

Malou Ravella, children's albums, writer


## FOOD

Carole Arogou (_À Croquer_), pastry chef

Élise Augier (_Domaine Augier_), wines

Nathalie Bagnus (_La petite ferme_), farm produce

Patricia and Patrice Bounichou, Johann Roth (_Ô délices de la Gi_), sweet and savoury cakes

Fred Bruno (Val d'abeille), honey

Cathy Carizzoni (Les jardins du blé vert), vegetables

Mélanie Cassard (_La cabane à safran_), vegetables, eggs, saffron

Guilhem Codera (_Terroir 06_), olives and citrus fruit

Sarah de Carqueray (_Sarah et Nicolas_), jam and ice cream

Sandrine Crisci (_Confiserie Crisci_), nougat and praline

Thomas Dubreuck (_La cerf-voise_), beer

Élise Durand (_Des abeilles et des fruits_), red fruit and honey

Marion Favaro (_L'oustal de Marion_), jam and honey

Jean-Pierre Fenocchio (_Côteaux Saint-Paulois_), jam and apple juice

Jéremy Laurenti (Chez Marco), charcuterie

Sylvain Marie (La Ruche de Valdeblore), honey

Alain Milano (_Sobal-J_), aperitif / bagnet

Fabienne Ramin (Fabienne's cakes), cakes

Philippe Raymondo, jam, eggs

Geneviève Richard (_Via florae_), medicinal plants

Amélie Rigot (_Les utopistes_), vegetables

Dominique Foucard (_La Ferme du Mirail_), vegetables

Céline Rosenweig, (_Produits locaux de la Tinée_), saffron

Romane Sattamino (_Les ailes rouges_), saffron honey

Céline Tamain (_L'univers d'Artemis_), medicinal plants

Véronique Thirion (_Aux escargots gourmands_), snails

Mary Voarino (_Naturo'life_), Mercantour herbal tea

Charlène Davy (_Les herbes folles_), jams and soups

Alice Lemut (_Un brin de Provence_) jams

Paul & Stark (_Chocolaterie du Pays Nicois_), chocolate

Yves Scarsini (_L'olivette_), olives and olive products


## DECORATION - HEALTH

Stéphane Aimé (_La galerie de topa_), sculpted sticks

Michelle Arsan (_Les détours du bois_), decoration and wooden toys

Gregory Biagioli (_Paracorde_), rope items

Virginie Barbera (_Vithal'créa & mimosas_), decoration

Christine Bertho (_Crystal création_), costume jewellery

Chantal Blais, wood painting

Amandine Digel (_Les ânes de Lara_), soap

Véronique Dutay (_Au gré des bois_), wood objects/painting

Emanuel Gomes (_Cocolive from Brazil_), olive wood

Patricia Hulin (_Atelier Abella_), lamps and decorative objects

Nadine Jeannot, animal paintings

Caroline Lelo, nature under glass, terrarium

Nicolas Merlin (_Savonnerie de la Tinée_), soap

Lila Ohlalalila, ceramics and crafts
o
Jacqueline Pastori, interior decoration, headbands, scrunchies

Aline Sigust, object decorations

Jean Pierre Vautherot, basket maker

Tanya Zollino (_Arts & Stones_), jewellery

Guillaume (_Le lycée de la Montagne_), woodwork

Christine Begole (_Le cadeau du cerf_), decorative objects


## WOOL

Patricia Chalot (_Na Va Ma_), felting

Nathalie Gastaldi (_Nata'laine_), wool & skeins

Mireille Marin (_Mimidou_), crocheted animals

Lydia Navez (_Les dentellières du Mercantour_), laces

## VEGETABLES

Claude Antoniazzi (_Pépiniére de la Madone_), nurseryman

## SUSTAINABLE LIFE

Cathy & Sandra (_Friperie des Vallées_), 2nd hand clothes

Lisa, Eric, Pierre-Antoine (_Collectif transition en Vésubie - Valdeblore_), sustainable transition in our valleys

## LEISURE

Bernard Baldassare (Les chemins d'azur), mountain leaders

## EVENTS - INSTITUTIONS

Denis Avon (_La ludothèque_), children's games

Martine Donato (_Les bulles d'arts_), drawing and face painting

Emmanuel Durst, goose driving

Virginie Favier (_Compost'n Co_), composting

Marie Gonthier (_CERPAM_), mountain management

Valentine Guerin (_On vous dit Patou_), information about patoos

Romain Lacoste (Mercantour National Park), environment

Jessica, (_Jeunes Agriculteurs d'Alpes Maritimes_), farmers' professional association

-->

<!---
<iframe src="https://www.azimut.fr/videos/video.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Presentation of the producers of the Transhumance Market 2022 1/2</iframe>

<iframe src="https://www.azimut.fr/videos/video2.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Transhumance Market 2022 Producer Presentation 2/2</iframe>
</p>
--->

<p>
<iframe src="https://www.azimut.fr/videos/video.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Présentation des producteurs du marché de la transhumance 1/2</iframe>

<iframe src="https://www.azimut.fr/videos/video2.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">Présentation des producteurs du marché de la transhumance 2/2</iframe>
</p>


![A cheese maker](/images/fromager.png)