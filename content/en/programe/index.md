---
title: "Program"
slug: "program"
description: "Course of the day"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 1
---


Valdeblore, a commune situated between 399 and 2674 m of altitude, has all the transhumant animals.

On this day, the 350 sheep of the GAEC des Éleveurs du Baous
<!--, the horses of the Centre équestre du Mercantour, Lionel's goats, the cows of the Mercantour farm and the donkeys of the ânes de blore will parade between the villages of la Bolline -->
 will walk between Bois Noir and the Soum del Pra in St Dalmas de Valdeblore.

![Sheep herd](/images/ovale_troupeau.png)