---
title: "Transhumance in Valdebore 2024"
slug: "transhumance-in-valdebore-2024"

description: "Meet again breeders 'Eleveurs des Baous' on saturday 29th June."
cascade:
  featured_image: '/images/bandeau-accueil.jpg'
---
<p>
<iframe src="https://www.azimut.fr/videos/2022-06-27fr3.mp4" style="border:none;overflow:hidden" scrolling="auto" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">FR3 Côte d'Azur report broadcasted on June 27, 2022</iframe>
</p>

### Transhumance...

Ovine transhumance from Provence to the Alps is the title of a very complete brochure, illustrated with maps and photos, offered by the Maison de la transhumance, located in Salon de Provence:

https://www.transhumance.org/

The word transhumance integrates two pieces of information, "trans" (beyond) and "humus" (the country), because the journey it designates leads beyond the territory of origin.

The authors explain how transhumance is a model of complementarity between the mountains and the plains. The multiple forms of transhumance are presented.

This type of farming, which is governed by the natural cycles of the grass and the animal, guarantees high-quality production.

While adapting to the changes in society, the breeders have managed to keep the authentic character of the breeding.

A map shows the summering places from the plains to the Mercantour and Mont Blanc massifs.

![Map of sheep transhumance](/images/carte-transhumances-ovines.png)

The profession of shepherd, with its specific know-how, is presented.
From the coastal plains to the Alpine mountains, transhumant shepherds use environmentally friendly practices.

The issue of pastoral breeding threatened by wolves is also addressed.
Finally, a cross-border agritourism route is being set up: "La Routo. In the footsteps of the transhumance"

https://larouto.eu/

Download the brochure:

https://www.transhumance.org/documentation-et-telechargement/


Information distributed with the kind permission of the Maison de la Transhumance.


![CERPAM logo](/logos/logo_cerpam64.png)
A similar study is proposed by the CERPAM, Centre d'Etudes et de Réalisations Pastorales Alpes-Méditerranée : "une transhumance, des transhumances" :

https://cerpam.com/les-transhumances/


Anne-Marie Destefanis
