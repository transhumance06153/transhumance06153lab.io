---
title: "Partners"
slug: partners
description: "Our partners and sponsors"
featured_image: "/images/bandeau-accueil.jpg"
menu:
  main:
    weight: 5
---

# Transhumance in Valdeblore

## June 29 2024

Thank you for your participation, help and support.


### Breeders :

- [Le GAEC des éleveurs des Baous](https://www.facebook.com/gaec.eleveursdesbaous)

### Shopkeepers, craftsmen and associations :


### Advertisers :

- [Le Trait d’union](https://traitdunion06.wordpress.com/)
- [Le Portail Vésubien](http://www.vesubian.com/index.html)
- [Nice-Matin - D. GASTALDI](https://www.nicematin.com/commune/valdeblore)
- [OTM la Colmiane Valdeblore](https://www.colmiane.com/)
- [France Bleu Azur](https://www.francebleu.fr/provence-alpes-cote-dazur/alpes-maritimes-06/valdeblore-06153)
- [Radio Emotion](https://radioemotion.fr/)

### Banks

- [Crédit  Agricole Provence - Côte d'Azur](https://www.credit-agricole.fr/ca-pca)

### Local authorities :

- [Mairie de Valdeblore](https://ville-valdeblore.fr/)
- [Mairie de Rimplas](https://www.ville-rimplas.fr/)
- [Métropole Nice-Côte d’Azur](https://www.nicecotedazur.org/contenu/les-communes/valdeblore/43)
- [Conseil départemental des Alpes-Maritimes](https://www.departement06.fr/patrimoine-par-commune/valdeblore-2338.html)
- [Région PACA](https://www.maregionsud.fr/)

### Other associations and public organizations

All our exhibitors

All our visitors

And all the dynamic volunteers of the Transhumance in Valdeblore.